import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        MutliplicationTable mutliplicationTable = new MutliplicationTable();
        String errorStr = "Invalid range!";

        // valid range
        Optional<String> result = mutliplicationTable.createMultiplicationTable(2, 9);
        System.out.println(result.orElse(errorStr));

        // not in range

        // start not in range
        result = mutliplicationTable.createMultiplicationTable(0, 888);
        System.out.println(result.orElse(errorStr));

        // end not in range
        result = mutliplicationTable.createMultiplicationTable(1, 1500);
        System.out.println(result.orElse(errorStr));

        // both start and end not in range
        result = mutliplicationTable.createMultiplicationTable(0, 1500);
        System.out.println(result.orElse(errorStr));

        // start number bigger than end number
        result = mutliplicationTable.createMultiplicationTable(9, 2);
        System.out.println(result.orElse(errorStr));
    }
}