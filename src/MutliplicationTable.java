import java.util.Optional;

public class MutliplicationTable {

    public Optional<String> createMultiplicationTable (int from, int to) {
        return isValid(from, to)? Optional.ofNullable(generateTableByRange(from, to)): Optional.empty();
    }

    public boolean isValid (int from, int to) {
        return isInRange(from, to) && isMinNoBiggerThanMax(from, to);
    }

    public boolean isInRange (int from, int to) {
        return (1 <= from && from <= 1000) && (1 <= to && to <= 1000);
    }

    public boolean isMinNoBiggerThanMax (int from, int to) {
        return from <= to;
    }

    public String generateTableByRange (int from, int to) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = from; i <= to; i++) {
            for(int j = from; j <= i; j++) {
                stringBuilder.append(String.format("%d*%d=%d", j, i, j*i));
                if (j != i) {
                    stringBuilder.append("  ");
                }
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
